import App from "./app";

(async () => {
  const PORT = Number(process.env.PORT) || 4500
  const instance = await App()

  await instance.listen(PORT, '0.0.0.0')
})()
