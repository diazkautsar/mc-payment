import {
  FastifyInstance,
  FastifyRequest,
  FastifyReply,
  FastifyError,
} from 'fastify'
import User from '../entities/user.entity'

type Authenticate = (
  instance: FastifyInstance
) => (
  this: FastifyInstance,
  request: FastifyRequest,
  reply: FastifyReply,
  ) => Promise<unknown>

export const authenticate: Authenticate = (fastify) => async function (request, reply) {
  const { authorization } = request.headers

  const token = authorization?.split(' ')[1]
  
  if (!authorization || !authorization?.startsWith('Bearer') || !token) {
    return reply.unauthorized()
  }

  try {
    const verify: any = this.jwt.verify(this, token || '')
    const user = await this.ORM.em.findOne(User, { id: verify.id })
    fastify.user.data = user
  } catch (error: any) {
    reply.unauthorized(error)
  }
}
