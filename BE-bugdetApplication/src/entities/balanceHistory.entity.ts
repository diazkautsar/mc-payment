import {
  BaseEntity,
  BigIntType,
  Entity, ManyToOne, PrimaryKey, Property
} from "@mikro-orm/core";
import { V4Options } from "uuid";
import BalanceType from "./balanceType.entity";
import User from "./user.entity";

@Entity({ tableName: 'balance_history' })
export default class BalanceHistory extends BaseEntity<BalanceHistory, 'id'> {
  @PrimaryKey({ type: String })
  id!: String | V4Options

  @Property({ fieldName: 'description' })
  description!: String | undefined

  @Property({ fieldName: 'balance_type_id' })
  balance_type_id!: String | V4Options

  @Property({ fieldName: 'user_id', type: BigIntType })
  user_id!: BigIntType | any

  @Property({ fieldName: 'amount', type: Number })
  amount!: Number

  @ManyToOne(() => User)
  user!: User

  @ManyToOne(() => BalanceType)
  balance_type!: BalanceType

  @Property({ fieldName: "created_at", type: Date })
  created_at: Date | null = new Date()

  @Property({ fieldName: "updated_at", type: Date, onUpdate: () => new Date() })
  updated_at: Date | null = new Date()
}