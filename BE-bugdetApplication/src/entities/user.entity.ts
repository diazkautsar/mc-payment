import {
  BaseEntity,
  BigIntType,
  Collection,
  Entity,
  OneToMany,
  PrimaryKey,
  Property
} from '@mikro-orm/core'
import BalanceHistory from './balanceHistory.entity'

@Entity({ tableName: 'user' })
export default class User extends BaseEntity<User, 'id'> {
  @PrimaryKey({ type: BigIntType })
  id!: Number

  @Property({ fieldName: "first_name", length: 100 })
  first_name!: string

  @Property({ fieldName: "last_name", length: 100 })
  last_name!: string | undefined

  @Property({ fieldName: "email", length: 100 })
  email!: string

  @Property({ fieldName: "phone", length: 100 })
  phone!: string | null

  @Property({ fieldName: "password" })
  password!: string

  @OneToMany({ entity: () => BalanceHistory, mappedBy: 'user', orphanRemoval: true })
  balance_history = new Collection<BalanceHistory>(this)

  @Property({ fieldName: "created_at", type: Date })
  created_at: Date | null = new Date()

  @Property({ fieldName: "updated_at", type: Date, onUpdate: () => new Date() })
  updated_at: Date | null = new Date()
}
