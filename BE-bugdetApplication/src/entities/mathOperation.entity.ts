import {
  BaseEntity,
  Collection,
  Entity,
  OneToMany,
  PrimaryKey,
  Property,
} from "@mikro-orm/core";
import { V4Options } from "uuid";
import BalanceType from "./balanceType.entity";

@Entity({ tableName: 'math_operation' })
export default class MathOperation extends BaseEntity<MathOperation, 'id'> {
  @PrimaryKey({ type: String })
  id!: String | V4Options

  @Property({ fieldName: 'title', length: 50 })
  title!: string

  @Property({ fieldName: 'symbol', length: 20 })
  symbol!: string

  @OneToMany({ entity: () => BalanceType, mappedBy: 'math_operation' })
  balance_type =  new Collection<BalanceType>(this)

  @Property({ fieldName: "created_at", type: Date })
  created_at: Date | null = new Date()

  @Property({ fieldName: "updated_at", type: Date, onUpdate: () => new Date() })
  updated_at: Date | null = new Date()
}