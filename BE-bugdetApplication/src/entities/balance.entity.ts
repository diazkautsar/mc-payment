import {
  BaseEntity,
  Entity,
  OneToOne,
  PrimaryKey,
  Property
} from "@mikro-orm/core";

import { V4Options } from 'uuid'
import User from "./user.entity";

@Entity({ tableName: 'balance' })
export default class Balance extends BaseEntity<Balance, 'id'> {
  @PrimaryKey({ type: String })
  id!: String | V4Options

  @Property({ fieldName: 'amount', type: BigInt })
  amount!: number

  @Property({ fieldName: 'user_id', type: BigInt })
  user_id!: number | any

  @Property({ fieldName: "created_at", type: Date })
  created_at: Date | null = new Date()

  @Property({ fieldName: "updated_at", type: Date, onUpdate: () => new Date() })
  updated_at: Date | null = new Date()
}