import {
  BaseEntity,
  Collection,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryKey,
  Property
} from '@mikro-orm/core'
import { V4Options } from 'uuid'
import BalanceHistory from './balanceHistory.entity'
import MathOperation from './mathOperation.entity'

@Entity({ tableName: 'balance_type' })
export default class BalanceType extends BaseEntity<BalanceType, 'id'> {
  @PrimaryKey({ type: String })
  id!: String | V4Options

  @Property({ fieldName: 'name', length: 100 })
  name!: String

  @Property({ fieldName: 'slug', length: 100 })
  slug!: String

  @ManyToOne(() => MathOperation)
  math_operation!: MathOperation

  @Property({ fieldName: 'description' })
  description!: String | undefined

  @Property({ fieldName: 'math_operation_id' })
  math_operation_id!: V4Options | String

  @OneToMany({ entity: () => BalanceHistory, mappedBy: 'balance_type', orphanRemoval: true })
  balance_history = new Collection<BalanceHistory>(this)

  @Property({ fieldName: "created_at", type: Date })
  created_at: Date | null = new Date()

  @Property({ fieldName: "updated_at", type: Date, onUpdate: () => new Date() })
  updated_at: Date | null = new Date()
}
