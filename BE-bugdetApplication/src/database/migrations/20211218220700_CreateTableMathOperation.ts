import { Migration } from '@mikro-orm/migrations'

export default class Migration_20211218220700_CreateTableMathOperation extends Migration {
  async up(): Promise<void> {
    const knex = this.getKnex()
    const schema = knex.schema

    schema.createTable('math_operation', (t) => {
      t.uuid('id').primary().unique().notNullable()
      t.string('title', 50).unique().notNullable()
      t.string('symbol', 20).unique().notNullable()

      t.timestamp("created_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("updated_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("deleted_at").nullable();
    })

    this.addSql(schema.toQuery())
  }

  async down(): Promise<void> {
    const schema = this.getKnex().schema

    schema.dropTableIfExists('math_operation')

    this.addSql(schema.toQuery())
  }
}
