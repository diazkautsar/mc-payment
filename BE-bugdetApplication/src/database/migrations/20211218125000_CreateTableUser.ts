import { Migration } from '@mikro-orm/migrations'

export default class Migration_20211218125000_CreateTableUser extends Migration {
  async up() {
    const knex = this.getKnex()
    const schema = knex.schema

    schema.createTable('user', (t) => {
      t.bigIncrements('id').primary().notNullable()
      t.string('first_name', 100).notNullable()
      t.string('last_name', 100).nullable()
      t.string('email', 100).notNullable()
      t.string('phone', 100).notNullable()
      t.string('password').notNullable()
      t.timestamp("created_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("updated_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("deleted_at").nullable();
    })

    this.addSql(schema.toQuery())
  }

  async down(): Promise<void> {
    const schema = this.getKnex().schema

    schema.dropTableIfExists('user')

    this.addSql(schema.toQuery())
  }
}
