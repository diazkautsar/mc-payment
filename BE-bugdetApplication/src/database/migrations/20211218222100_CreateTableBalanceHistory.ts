import { Migration } from '@mikro-orm/migrations'

export default class Migration_20211218222100_CreateTableBalanceHistory extends Migration {
  async up(): Promise<void> {
    const knex = this.getKnex()
    const schema = knex.schema

    schema.createTable('balance_history', (t) => {
      t.uuid('id').primary().notNullable().unique()
      t.uuid('balance_type_id').notNullable()

      t.foreign('balance_type_id')
        .references('id')
        .inTable('balance_type')
        .onDelete('CASCADE')
        .onUpdate('CASCADE')
      
      t.bigInteger('user_id').notNullable()

      t.foreign('user_id')
        .references('id')
        .inTable('user')
        .onDelete('CASCADE')
        .onUpdate('CASCADE')
      
      t.string('description').nullable()

      t.timestamp("created_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("updated_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("deleted_at").nullable();
    })

    this.addSql(schema.toQuery())
  }

  async down(): Promise<void> {
    const schema = this.getKnex().schema

    schema.dropTableIfExists('balance_history')

    this.addSql(schema.toQuery())
  }
}
