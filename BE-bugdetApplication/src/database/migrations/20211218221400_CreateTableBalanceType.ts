import { Migration } from '@mikro-orm/migrations'

export default class Migration_20211218221400_CreateTableBalanceType extends Migration {
  async up(): Promise<void> {
    const knex = this.getKnex()
    const schema = knex.schema

    schema.createTable('balance_type', (t) => {
      t.uuid('id').primary().notNullable().unique()
      t.string('name', 100).notNullable().unique()
      t.string('slug', 100).notNullable().unique()
      t.string('description').nullable()
      t.uuid('math_operation_id').notNullable()

      t.foreign('math_operation_id')
        .references('id')
        .inTable('math_operation')
        .onDelete('CASCADE')
        .onUpdate('CASCADE')

      t.timestamp("created_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("updated_at").nullable().defaultTo(knex.fn.now());
      t.timestamp("deleted_at").nullable();
    })

    this.addSql(schema.toQuery())
  }

  async down(): Promise<void> {
    const schema = this.getKnex().schema

    schema.dropTableIfExists('balance_type')

    this.addSql(schema.toQuery())
  }
}
