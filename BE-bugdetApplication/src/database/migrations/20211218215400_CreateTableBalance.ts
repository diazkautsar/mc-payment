import { Migration } from '@mikro-orm/migrations'

export default class Migration_20211218215400_CreateTableBalance extends Migration {
  async up(): Promise<void> {
    const knex = this.getKnex()
    const schema = knex.schema

    schema.createTableIfNotExists('balance', (t) => {
      t.uuid('id').primary().unique().notNullable()
      t.integer('amount').notNullable().unique()
      t.bigInteger('user_id').notNullable()

      t.foreign('user_id')
        .references('id')
        .inTable('user')
        .onDelete('CASCADE')
        .onUpdate('CASCADE')
      
      t.timestamp("created_at").nullable().defaultTo(knex.fn.now())
      t.timestamp("updated_at").nullable().defaultTo(knex.fn.now())
      t.timestamp("deleted_at").nullable()
    })

    this.addSql(schema.toQuery())
  }

  async down(): Promise<void> {
    const schema = this.getKnex().schema

    schema.dropTableIfExists('balance')

    this.addSql(schema.toQuery())
  }
}
