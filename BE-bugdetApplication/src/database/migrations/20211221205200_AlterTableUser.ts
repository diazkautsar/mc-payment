import { Migration } from '@mikro-orm/migrations'

export default class Migration_20211221205200_AlterTableBalanceHistory extends Migration {
  async up(): Promise<void> {
    const kenx = this.getKnex()
    const schema = kenx.schema

    schema.alterTable('balance_history', (t) => {
      t.integer('amount').notNullable().defaultTo(0)
    })

    this.addSql(schema.toQuery())
  }
}
