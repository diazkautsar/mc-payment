import { FastifyPluginAsync } from 'fastify'

import {
  registerController,
  loginController,
} from './controllers'

import {
  registerSchema,
  loginSchema,
} from './schema'

const skipOverride = Symbol.for('skip-override')

const authRoutes: FastifyPluginAsync & { [skipOverride]?: boolean } = async (instance) => {
  instance.route({
    method: 'POST',
    schema: registerSchema,
    url: '/auth/signup',
    handler: registerController,
  })

  instance.route({
    method: 'POST',
    schema: loginSchema,
    url: '/auth/signin',
    handler: loginController,
  })
}

authRoutes[skipOverride] = true

export default authRoutes
