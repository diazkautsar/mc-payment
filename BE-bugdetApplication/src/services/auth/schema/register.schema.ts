import { Type } from '@sinclair/typebox'

export const registerSchema = {
  tags: ['Authentication'],
  summary: 'Authentication endpoint',
  description: 'Register user',
  body: Type.Object({
    first_name: Type.String(),
    last_name: Type.Optional(Type.String()),
    email: Type.String(),
    password: Type.String(),
    phone: Type.String(),
  }),
  response: {
    201: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Optional(Type.Any()),
    })
  }
}
