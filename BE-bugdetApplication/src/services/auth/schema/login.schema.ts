import { Type } from '@sinclair/typebox'

export const loginSchema = {
  tags: ['Authentication'],
  summary: 'Authentication endpoint',
  description: 'Login user',
  body: Type.Object({
    phone: Type.Optional(Type.String()),
    email: Type.Optional(Type.String()),
    password: Type.String()
  }),
  response: {
    200: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Optional(Type.Any()),
    })
  }
}
