import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'
import { compareSync } from 'bcrypt'

import User from '../../../entities/user.entity'
import { loginSchema } from '../schema'
import { normalizePhoneNumber } from '../../../utils'

type LoginRouteHandler = RouteHandler<{
  Reply: Static<typeof loginSchema.response[200]>
  Body: Static<typeof loginSchema.body>
}>

const loginController: LoginRouteHandler = async function (request, reply) {
  try {
    const { email, phone, password } = request.body

    const user = await this.ORM.em.findOne(
      User,
      { $or: [{ email }, { phone: normalizePhoneNumber(phone, true) }] },
    )

    if (!user) {
      throw this.httpErrors.badRequest('user not found')
    }

    const comparePassword = compareSync(password, user.password)

    if (!comparePassword) {
      throw this.httpErrors.badRequest('invalid email / phone and password')
    }

    const signToken = this.jwt.sign(this, user)

    return {
      statusCode: '200',
      message: 'Login success',
      data: {
        access_token: signToken.token,
        expires_ind: signToken.expiresIn,
      }
    }
  } catch (error) {
    throw error
  }
}

export default loginController
