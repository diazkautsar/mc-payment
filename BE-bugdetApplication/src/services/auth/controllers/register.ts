import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'
import { hashSync, genSaltSync } from 'bcrypt'

import User from '../../../entities/user.entity'
import { registerSchema } from '../schema'
import { BcryptSaltRounds } from '../../../config'
import { normalizePhoneNumber } from '../../../utils'

type RegisterRouteHandler = RouteHandler<{
  Reply: Static<typeof registerSchema.response[201]>
  Body: Static<typeof registerSchema.body>
}>

const registerController: RegisterRouteHandler = async function (request, reply) {
  try {
    const { first_name, last_name, email, password, phone } = request.body

    const existingUser = await this.ORM.em.findOne(
      User,
      { $or: [{ email }, { phone: normalizePhoneNumber(phone, true) }] },
      { fields: ['email', 'phone'] }
    )

    if (existingUser) {
      const type = existingUser.phone === normalizePhoneNumber(phone, true) ? 'phone' : 'email'
      const errorMessage = type === 'phone' ? 'Phone number already registered' : 'Email address already registered'

      throw this.httpErrors.unprocessableEntity(errorMessage)
    }

    const salt = genSaltSync(BcryptSaltRounds.salt)
    const hash = hashSync(password, salt)

    const user = new User
    user.first_name = first_name
    user.last_name = last_name
    user.email = email
    user.password = hash
    user.phone = normalizePhoneNumber(phone, true)

    await this.ORM.em.persistAndFlush([user])

    return {
      statusCode: '201',
      message: `Register account for email: ${email} success`,
    }
  } catch (error) {
    throw error
  }
}

export default registerController
