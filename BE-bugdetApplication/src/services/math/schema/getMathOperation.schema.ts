import { Type } from '@sinclair/typebox'

export const getMathOperationSchema = {
  tags: ['Math Operation'],
  summary: 'Math Operation endpoint',
  description: 'Use this endpoint for CRUD math operation table',
  response: {
    200: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Optional(Type.Any())
    })
  }
}
