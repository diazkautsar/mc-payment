import { Type } from '@sinclair/typebox'

export const addMathOperationSchema = {
  tags: ['Math Operation'],
  summary: 'Math Operation endpoint',
  description: 'Use this endpoint for CRUD math operation table',
  body: Type.Array(Type.Object({
    title: Type.String(),
    symbol: Type.String()
  })),
  response: {
    201: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Optional(Type.Any()),
    })
  }
}
