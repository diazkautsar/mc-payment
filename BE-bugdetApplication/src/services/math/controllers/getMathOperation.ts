import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'

import MathOperation from '../../../entities/mathOperation.entity'
import { getMathOperationSchema } from '../schema'

type GetMathOperationRouteHandler = RouteHandler<{
  Reply: Static<typeof getMathOperationSchema.response[200]>
}>

const getMathOperationController: GetMathOperationRouteHandler = async function (request, reply) {
  try {
    const data = await this.ORM.em.find(MathOperation, {})
    
    reply.send({
      statusCode: '200',
      message: 'success fetch math operation',
      data,
    })
  } catch (error) {
    throw error
  }
}

export default getMathOperationController
