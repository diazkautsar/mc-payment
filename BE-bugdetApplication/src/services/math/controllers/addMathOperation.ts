import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'
import { v4 } from 'uuid'

import { addMathOperationSchema } from '../schema'

type AddMathOperationRouteHandler = RouteHandler<{
  Reply: Static<typeof addMathOperationSchema.response[201]>
  Body: Static<typeof addMathOperationSchema.body>
}>

const addMathOperationController: AddMathOperationRouteHandler = async function (request, reply) {
  try {
    const data = request.body

    if (!data.length) {
      throw this.httpErrors.badRequest('body request at least have one object data')
    }

    const promises: any[] = []

    data.forEach(async item => {
      const query = `
      INSERT INTO math_operation (id, title, symbol)
      VALUES ('${v4()}', '${item.title}', '${item.symbol}')
      `

      promises.push(this.ORM.em.getConnection().execute(query))
    })

    await Promise.all(promises).catch((err) => {
      throw this.httpErrors.badRequest(err.detail)
    })

    return {
      message: 'success add math operation',
      statusCode: '201'
    }
  } catch (error) {
    throw error
  }
}

export default addMathOperationController
