import { FastifyPluginAsync } from 'fastify'

import {
  addMathOperationController,
  getMathOperationController
} from './controllers'

import {
  addMathOperationSchema,
  getMathOperationSchema
} from './schema'

const skipOverride = Symbol.for('skip-override')

const mathOperationRoutes: FastifyPluginAsync & { [skipOverride]?: boolean } = async (instance) => {
  instance.route({
    method: 'POST',
    schema: addMathOperationSchema,
    url: '/math',
    handler: addMathOperationController,
  })

  instance.route({
    method: 'GET',
    schema: getMathOperationSchema,
    url: '/math',
    handler: getMathOperationController,
  })
}

mathOperationRoutes[skipOverride] = true

export default mathOperationRoutes
