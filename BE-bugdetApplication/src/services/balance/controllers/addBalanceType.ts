import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'
import { v4 } from 'uuid'

import BalanceType from '../../../entities/balanceType.entity'
import { addBalanceTypeSchema } from '../schema'

import { BalanceUtils } from '../../../utils'

type AddBalanceTypeRouteHandler = RouteHandler<{
  Reply: Static<typeof addBalanceTypeSchema.response[201]>
  Body: Static<typeof addBalanceTypeSchema.body>
}>

const addBalanceTypeController: AddBalanceTypeRouteHandler = async function (request, reply) {
  try {
    const { math_operation_id, name, description } = request.body

    const utils = new BalanceUtils()
    const slug = utils.createSlugBalanceType(name)

    const balanceType = new BalanceType
    balanceType.id = v4()
    balanceType.name = name
    balanceType.slug = slug
    balanceType.description = description
    balanceType.math_operation_id = math_operation_id

    await this.ORM.em.persistAndFlush([balanceType]).catch((err) => {
      throw this.httpErrors.badRequest(err.detail)
    })

    reply.send({
      message: 'success add balance type',
      statusCode: '201'
    }).code(201)
  } catch (error) {
    throw error
  }
}

export default addBalanceTypeController
