import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'

import { addBalanceSchema } from '../schema'
import { BalanceUtils } from '../../../utils'
import BalanceHistory from '../../../entities/balanceHistory.entity'
import Balance from '../../../entities/balance.entity'
import { v4 } from 'uuid'

type AddBalanceRouteHandler = RouteHandler<{
  Reply: Static<typeof addBalanceSchema.response[201]>
  Body: Static<typeof addBalanceSchema.body>
}>

const utils = new BalanceUtils()

const addBalanceController: AddBalanceRouteHandler = async function (request, reply) {
  try {
    const { balance_type_id, description, amount } = request.body

    const existingData = await utils.getExistingBalance(this, this.user.data?.id)

    if (!existingData) {
      const balance = new Balance()
      balance.id = v4()
      balance.amount = amount
      balance.user_id = Number(this.user.data?.id)

      await this.ORM.em.persistAndFlush([balance]).catch(err => {
        throw this.httpErrors.badRequest(err.detail)
      })
    } else {
      const math_operation = await utils.getMathOperationBasedOnBalanceTypeId(this, balance_type_id)
      const getAmountCalculation = await utils.calculateAmount(this, existingData.amount, amount, math_operation.symbol)

      const sql = `
        UPDATE balance
        SET amount = ${getAmountCalculation}
        WHERE balance.user_id = ${this.user.data?.id};
      `

      await this.ORM.em.getConnection().execute(sql).catch(err => {
        throw this.httpErrors.badRequest(err.detail)
      })
    }

    const balance_history = new BalanceHistory()
    balance_history.id = v4()
    balance_history.balance_type_id = balance_type_id
    balance_history.description = description
    balance_history.user_id = this.user.data?.id
    balance_history.amount = amount

    await this.ORM.em.persistAndFlush([balance_history]).catch(err => {
      throw this.httpErrors.badRequest(err.detail)
    })


    reply.send({
      message: 'update balance success',
      statusCode: '201',
    }).status(201)
  } catch (error) {
    throw error
  }
}

export default addBalanceController
