import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'

import { getBalanceHistorySchema } from '../schema'

type GetBalanceHistoryRouteHandler = RouteHandler<{
  Reply: Static<typeof getBalanceHistorySchema.response[200]>
}>

type queryType = {
  balance_history_id: string,
  balance_type_id: string,
  first_name: string,
  last_name: string,
  balance_history_description: string,
  balance_history_amount: number,
  balance_type_name: string,
  balance_type_slug: string,
  balance_type_description: string,
}

const getBalanceHIstoryController: GetBalanceHistoryRouteHandler = async function (request, reply) {
  try {
    const sql = `
      select
      bh.id as balance_history_id,
      bt.id as balance_type_id,
      u.first_name as first_name,
      u.last_name as last_name,
      bh.description as balance_history_description,
      bh.amount as balance_history_amount,
      bt."name" as balance_type_name,
      bt.slug as balance_type_slug,
      bt.description as balance_type_description
      from balance_history bh
      inner join balance_type bt on bh.balance_type_id = bt.id
      inner join "user" u
      on u.id = bh.user_id
      where u.id = '${Number(this.user.data?.id)}'
    `

    const data: queryType[] | any = await this.ORM.em.getConnection().execute(sql).catch(err => {
      throw this.httpErrors.badRequest(err.detail)
    })

    reply.send({
      message: 'success fecth data',
      statusCode: '200',
      data,
    }).status(200)
  } catch (error) {
    throw error
  }
}

export default getBalanceHIstoryController
