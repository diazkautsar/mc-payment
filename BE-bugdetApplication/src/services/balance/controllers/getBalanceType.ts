import { RouteHandler } from 'fastify'
import { Static } from '@sinclair/typebox'

import { getBalanceTypeSchema } from '../schema'

type GetBalanceTypeRouteHandler = RouteHandler<{
  Reply: Static<typeof getBalanceTypeSchema.response[200]>
}>

const getBalanceTypeController: GetBalanceTypeRouteHandler = async function (request, reply) {
  try {
    const sql = `select bt.id as balance_type_id, mo.id as math_operation_id,
    bt."name" as balance_type_name, bt.slug as balance_type_slug,
    mo.title as math_operation_title, mo.symbol as math_operation_symbol
    from balance_type bt
    inner join math_operation mo
    on bt.math_operation_id = mo.id;`

    const data = await this.ORM.em.getConnection().execute(sql).catch((err) => {
      throw this.httpErrors.badRequest(err.detail)
    })

    reply.send({
      message: 'success fetch balance type',
      statusCode: '200',
      data,
    }).status(200)
  } catch (error) {
    throw error
  }
}

export default getBalanceTypeController
