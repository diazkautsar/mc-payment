import { FastifyPluginAsync } from 'fastify'

import {
  addBalanceController,
  addBalanceTypeController,
  getBalanceHistoryController,
  getBalanceTypeController,
} from './controllers'

import {
  addBalanceSchema,
  addBalanceTypeSchema,
  getBalanceHistorySchema,
  getBalanceTypeSchema,
} from './schema'

import {
  authenticate
} from '../../middlewares'

const skipOverride = Symbol.for('skip-override')

const balanceRoutes: FastifyPluginAsync & { [skipOverride]?: boolean } = async (instance) => {
  instance.route({
    method: 'POST',
    preValidation: [ authenticate(instance) ],
    schema: addBalanceTypeSchema,
    url: '/balance/type',
    handler: addBalanceTypeController,
  })
  
  instance.route({
    method: 'GET',
    preValidation: [authenticate(instance)],
    schema: getBalanceTypeSchema,
    url: '/balance/type',
    handler: getBalanceTypeController
  })

  instance.route({
    method: 'POST',
    preValidation: [authenticate(instance)],
    schema: addBalanceSchema,
    url: '/balance',
    handler: addBalanceController,
  })

  instance.route({
    method: 'GET',
    preValidation: [authenticate(instance)],
    schema: getBalanceHistorySchema,
    handler: getBalanceHistoryController,
    url: '/balance/history'
  })
}

balanceRoutes[skipOverride] = true

export default balanceRoutes
