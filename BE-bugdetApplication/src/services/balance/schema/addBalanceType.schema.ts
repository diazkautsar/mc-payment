import { Type } from '@sinclair/typebox'

export const addBalanceTypeSchema = {
  tags: ['Balance'],
  summary: 'Balance endpoint',
  description: 'Use this endpoint for Balance',
  body: Type.Object({
    math_operation_id: Type.String(),
    name: Type.String(),
    description: Type.Optional(Type.String()),
  }),
  response: {
    201: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Optional(Type.Any()),
    })
  }
}
