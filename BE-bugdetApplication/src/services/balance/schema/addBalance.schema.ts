import { Type } from '@sinclair/typebox'

export const addBalanceSchema = {
  tags: ['Balance'],
  summary: 'Balance endpoint',
  description: 'Use this endpoint for Balance',
  body: Type.Object({
    balance_type_id: Type.String(),
    description: Type.Optional(Type.String()),
    amount: Type.Number()
  }),
  response: {
    201: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Optional(Type.Any()),
    })
  }
}
