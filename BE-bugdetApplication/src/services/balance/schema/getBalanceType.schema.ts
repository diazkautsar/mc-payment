import { Type } from '@sinclair/typebox'

export const getBalanceTypeSchema = {
  tags: ['Balance'],
  summary: 'Balance endpoint',
  description: 'Use this endpoint for Balance',
  response: {
    200: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Optional(Type.Any()),
    })
  }
}
