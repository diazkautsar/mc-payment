import { Type } from '@sinclair/typebox'

export const getBalanceHistorySchema = {
  tags: ['Balance'],
  summary: 'Balance endpoint',
  description: 'Use this endpoint for Balance',
  response: {
    200: Type.Object({
      statusCode: Type.String(),
      message: Type.String(),
      data: Type.Array(Type.Object({
        balance_history_id: Type.String(),
        balance_type_id: Type.String(),
        first_name: Type.String(),
        last_name: Type.String(),
        balance_history_description: Type.String(),
        balance_history_amount: Type.Number(),
        balance_type_name: Type.String(),
        balance_type_slug: Type.String(),
        balance_type_description: Type.String(),
      })),
    })
  }
}
