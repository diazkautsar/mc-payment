import fp from 'fastify-plugin';
import OAS, { FastifyOASOptions } from 'fastify-oas';

export default fp(async function (instance) {
  instance.register(OAS, {
    routePrefix: '_doc',
    hideUntagged: true,
    swagger: {
      info: {
        title: 'Budget Application',
        version: process.env.npm_package_version,
        contact: {
          name: "Diaz Kautsar",
          url: "https://diazkautsar.com",
          email: "diazkautsar77@gmai.com",
        },
      },
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [],
      definitions: {},
      securityDefinitions: {
        ApiToken: {
          description: 'Authorization header token, sample: "Bearer #TOKEN#" or "Server API key"',
          type: 'apiKey',
          name: 'Authorization',
          in: 'header'
        },
      },
    },
    exposeRoute: true,
  } as unknown as FastifyOASOptions);
});
