import { MikroORM, Options, RequestContext } from '@mikro-orm/core'
import { PostgreSqlDriver } from '@mikro-orm/postgresql'
import fp from 'fastify-plugin'

declare module 'fastify' {
  interface FastifyInstance {
    ORM: MikroORM<PostgreSqlDriver>
  }
}

export default fp(async (fastify, opts: Options) => {
  const mikroORM = await MikroORM.init({
    ...opts,
    findOneOrFailHandler: (entityName) => {
      return fastify.httpErrors.notFound(`${entityName} is not found`)
    }
  })

  fastify.decorate('ORM', mikroORM)
  fastify.addHook('preValidation', (_request, _reply, next) => {
    RequestContext.create(mikroORM.em, next)
  })
  fastify.addHook('onClose', async () => {
    await mikroORM.close()
  })
})
