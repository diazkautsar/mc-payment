import fastifyBlipp from 'fastify-blipp'
import fastifyCors from 'fastify-cors'
import fastifyPlugin from 'fastify-plugin'
import fastifySensible from 'fastify-sensible'
import fastifyHelmet from 'fastify-helmet'

import mikroORM from './MikroORM'
import jwt from './jwt'
import OAS from './OAS'

export default fastifyPlugin(async(instance) => {
  instance.register(fastifyBlipp)
  instance.register(fastifyCors)
  instance.register(fastifySensible)
  instance.register(OAS)
  instance.register(fastifyHelmet, {
    contentSecurityPolicy: {
      directives: {
        defaultSrc: ["'self'"], // default source is mandatory
        imgSrc: ["'self'", 'data:', 'validator.swagger.io'],
        scriptSrc: ["'self'"],
        styleSrc: ["'self'", 'https:'],
      }
    }
  })
  instance.register(mikroORM, instance.config.mikroORM)
  instance.register(jwt)
})
