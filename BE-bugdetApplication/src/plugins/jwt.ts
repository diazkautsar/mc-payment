import { FastifyInstance } from 'fastify'
import fp from 'fastify-plugin'
import jwt from 'jsonwebtoken'
import ms from 'ms'

import User from "../entities/user.entity";

export const sign = (instance: FastifyInstance, user: User) => {
  const expiresIn = ms(instance.config.auth.expiresIn as string) / 1000

  const token = jwt.sign(
    user.toJSON(),
    instance.config.auth.secretKey,
    { expiresIn }
  )

  return { token, expiresIn }
}

export const verify = (instance: FastifyInstance, token: string) => {
  return jwt.verify(token, instance.config.auth.secretKey)
}

export default fp(async (fastify) => {
  fastify.decorate('jwt', {
    sign,
    verify,
  })

  fastify.decorate('user', {
    data: User,
  })
})

declare module 'fastify' {
  interface FastifyInstance {
    jwt: {
      sign: typeof sign,
      verify: typeof verify,
    },
    user: {
      data: User | null,
    }
  }
}

