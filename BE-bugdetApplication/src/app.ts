require('dotenv-expand')(require('dotenv').config())

import fastify from 'fastify'
import Autoload from 'fastify-autoload'
import path from 'path'

import * as config from './config'
import plugins from './plugins'

const App = async () => {
  const NODE_ENV = process.env.NODE_ENV || 'development'

  const instance = fastify({
    logger: {
      level: NODE_ENV == 'test' ? 'error' : 'info',
      prettyPrint: true,
    },
    schemaErrorFormatter(errors, dataVar) {
      const error = errors[0]
      return new Error(`Property ${dataVar}${error.dataPath} ${error.message}`)
    }
  })

  instance.decorate('config', config)
  instance.register(plugins)

  instance.register(Autoload, {
    dir: path.resolve(__dirname, 'services'),
    ignorePattern: /.*.(test|spec|entity).(js|ts)/,
    maxDepth: 1
  })

  return instance
}

export default App

declare module 'fastify' {
  interface FastifyInstance {
    config: typeof config
  }
}
