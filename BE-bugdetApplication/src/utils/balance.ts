import { FastifyInstance } from 'fastify'
import { HttpError } from 'fastify-sensible/lib/httpError'

export type getMathOperationBasedOnBalanceTypeId = {
  id: string,
  name: string,
  slug: string,
  description: string,
  math_operation_id: string,
  created_at: string | Date,
  updated_at: string | Date,
  deleted_at: string | Date,
  title: string,
  symbol: string,
}

export default class BalanceUtils {
  createSlugBalanceType(param: string): string {
    return param.toLowerCase().split(' ').join('-')
  }

  async getMathOperationBasedOnBalanceTypeId(instance: FastifyInstance, balance_type_id: string): Promise<getMathOperationBasedOnBalanceTypeId | HttpError> {
    try {
      const sql = `select * from balance_type bt inner join math_operation mo on bt.math_operation_id = mo.id where bt.id = '${balance_type_id}';`

      const [data]: any = await instance.ORM.em.getConnection().execute(sql).catch(err => {
        throw instance.httpErrors.badRequest(err.detail)
      })

      return data
    } catch (error) {
      throw error
    }
  }

  async getExistingBalance(instance: FastifyInstance, user_id: any): Promise<any | HttpError> {
    try {
      const sql = `select * from balance b where b.user_id  = ${+user_id}`

      const [data]: any = await instance.ORM.em.getConnection().execute(sql).catch(err => {
        throw instance.httpErrors.badRequest(err.detail)
      })

      return data
    } catch (error) {
      throw error
    }
  }

  async calculateAmount(instance: FastifyInstance, existingAmount: number, amount: number, symbol: string): Promise<void | number | HttpError> {
    switch (symbol.toLowerCase()) {
      case '+':
        return existingAmount + amount
      
      case '-':
        return existingAmount - amount
      
      case '*':
        return existingAmount * amount
      
      case 'x':
        return existingAmount * amount
      
      case '/':
        return existingAmount / amount
    
      default:
        throw instance.httpErrors.badRequest('Math calculation symbol not found')
    }
  }
}
