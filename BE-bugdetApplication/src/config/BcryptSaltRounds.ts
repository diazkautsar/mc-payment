import env from '../libs/env'

export default {
  salt: env('BCRYPT_SALT') as number,
}