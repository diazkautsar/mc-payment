import env from '../libs/env'

export default {
  expiresIn: env('AUTH_EXPIRES_IN', '24h') as number | string,
  secretKey: env('AUTH_SECRET_KEY') as string,
}