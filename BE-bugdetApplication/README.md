# BACKEND FOR BADGET APPLICATION

## Requirement

- Node.js `>= 14.0.0`
- Fastify `>= 3.0.0`
- PostgresSQL `12`
- Typescript

# Installation

1. clone this repo to your localhost
2. move to root project BE Side
   ```
   cd BE-budgetApplication
   ```
3. Copy `.env.example` to `.env` or you can write your own env base on `./src/config/*.ts`
4. Install depedency
   ```bash
    yarn install
   ```
5. Run the migration script
   ```bash
    npm run db-migration:up
   ```
   > ⚠️ Before running migration script, make sure your database connection is connected
6. You are ready to go

# API - DOCUMENTATION

Api documentation can found with url:

```
http://localhost:{YOUR_ENV_PORT}/_doc
```

# Database Migrations

I use [mikro-orm](https://mikro-orm.io/docs/migrations) built on top of [knex.js](https://knexjs.org) as ORM and migration script runner.
You can use the following CLI command to run migration script

### Writing migration file

> ⚠️ We do not recommend you to use `npx mikro-orm migration:create` or `npm run db -- migration:create` command due bad naming file. Create the migration script yourself instead.

Migration file can be found on [`src/database/migrations/*.ts`](src/database/migrations/)

Use the following naming pattern for migration file
`${YYYYMMDDHHmmss}_${CamelCaseName}`

Example:

✅ `20201212180032_CreateUserTable.ts`

❌ `CreateUserTable_20201212180032.ts`
