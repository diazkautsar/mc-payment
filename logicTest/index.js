function twoSums(nums = [], target) {
  let result = []

  nums.forEach((item, indexFirstLoop) => {
    nums.forEach((value, indexSecondLoop) => {
      const findExisting = result.find(x => x === indexFirstLoop)
      const secondExisting = result.find(x => x === indexSecondLoop)

      if ((item + value === target) && (indexFirstLoop !== indexSecondLoop) && (!findExisting && !secondExisting)) {
        result = [ ...result, indexFirstLoop, indexSecondLoop]
      }
    })
  })
  
  return result
}


console.log(twoSums([2, 7, 11, 15], 13))
console.log(twoSums([3, 2, 4], 6))
console.log(twoSums([3, 3], 6))
